# Watermark

## Overview

Python scripts in this repository add watermark to given videos and images.
to be completed later

## How to Use

To add watermark to given inputs, you should specify the input directory, the output directory, and the watermark image.

In the case you want to add a watermark to the given images, you should run [watermark_image.py](watermark_image.py). If you want to add a watermark to the given videos, you should run [watermark_video.py](watermark_video.py).

To run the needed code, use the command below.
```
python watermark_(image/video) input_dir output_dir watermark_image_path
```
The `input_dir` should be the directory of input images or videos. The codes will get a list of all images or videos in the given directory and will add watermark to all of them. The `output_dir` should be the directory of the output images or videos. If you are adding watermark to a sample file `a.*`, the output will be `a_out.*`. The `watermark_image_path` should be the path of the watermark image you want to add.

The mentioned command contains the least parameters. Now, let us go through some important optional parameters.

The `opacity` arg controls the opacity of the watermark on the input. You should assign a value between zero and one (and equal to them). If you assign no value, it will be set to one. If you want to change the value, use command below.
```
python watermark_(image/video) input_dir output_dir watermark_image_path --opacity 0.7
```
or 
```
python watermark_(image/video) input_dir output_dir watermark_image_path -o 0.7
```

The `location` arg controls the location of watermark. The `location` arg can have `auto`, `top_left`, `top_right`, `bottom_left`, `bottom_right`, or `bottom_center` values. The default value is `auto`. If the location is chosen automatically, the codes use `find_best_location` function to find the best location to put the watermark there. The best location is determined in the first frame and used in the following frames.

To set the location, you can use the code below.
```
python watermark_(image/video) input_dir output_dir watermark_image_path --location top_right
```
or 
```
python watermark_(image/video) input_dir output_dir watermark_image_path -l top_right
```

If you need help with the args, you can use the help menu.
```
python watermark_(image/video) -h
```
## Requirements
The needed packages are listed in the [requirements.txt](requirements.txt) for the `watermark_video.py` code, and [requirements_image.txt](requirements_image.txt) for the `watermark_image.py` code.